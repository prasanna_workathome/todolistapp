using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GE.Business;
using GE.Business.Database;
using GE.Business.Dtos;
using GE.Business.Mapping;
using GE.Business.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using NUnit.Framework;

namespace ToDoListBusinessTest
{
    /// <summary>
    /// Unit Test for Business Layer
    /// </summary>
    public class ToDoListRepositoryTest
    {
        private  ToDoListContext _contextMock;
        private IMapper _mapper;
        private IToDoListRepository _toDoListRepository;

        [SetUp]
        public void Setup()
        {
            MappingManager.Initialize();
            _mapper = MappingManager.AutoMapper;
            _contextMock = SetupMockDbContext();
            _toDoListRepository = new ToDoListRepository(_contextMock);
        }

        [Test]
        public async Task GetAllToDoListTest()
        {
            var result = await _toDoListRepository.GetAllToDoList();
            Assert.That(result.Count,Is.GreaterThan(1));
        }

        [Test]
        public async Task GetToDoListDetailsTest()
        {
            var id = _contextMock.ToDoListItemBaseDto.FirstOrDefault(x => x.Name == "Code Review").Id;
            var result = await _toDoListRepository.GetToDoListById(id);
            Assert.That(result.Id ==id, Is.True);
        }

        [Test]
        public async Task AddToDoListTest()
        {
            var toDoListDetailsQueryDto = new ToDoListDetailsQueryDto {Name = "Pull request review"};

            var result = await _toDoListRepository.Add(toDoListDetailsQueryDto);
            Assert.That(result.Id, Is.Not.EqualTo(Guid.Empty));
        }

        [Test]
        public async Task UpdateToDoListTest()
        {
            var id = _contextMock.ToDoListItemBaseDto.FirstOrDefault(x=>x.Name== "Code Review").Id;
            var result = await _toDoListRepository.Update(id);
            Assert.That(result.IsCompleted, Is.EqualTo(true));
        }

        /// <summary>
        /// Mock context set up
        /// </summary>
        /// <returns></returns>
        private ToDoListContext SetupMockDbContext()
        {
            var options = new DbContextOptionsBuilder<ToDoListContext>()
                .UseInMemoryDatabase(databaseName: "ToDoList")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
            var context = new ToDoListContext(options);
            context.ToDoListItemBaseDto.AddRange(ToDoListSeeData.GetToDoList());
            context.SaveChanges();
            return context;
        }

        /// <summary>
        /// Seed Test Data
        /// </summary>
        private static class ToDoListSeeData
        {
            public static List<ToDoListItemBaseDto> GetToDoList()
            {
                var items = new List<ToDoListItemBaseDto>();
                items.Add(new ToDoListDetailsQueryDto()
                {
                    Id = Guid.NewGuid(),
                    Name = "Code Review"
                });
                items.Add(new ToDoListDetailsQueryDto()
                {
                    Id = Guid.NewGuid(),
                    Name = "Create Specification"
                });
                return items;
            }
        }
    }
}