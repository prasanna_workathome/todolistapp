﻿using FluentValidation;
using GE.Business;

namespace GE.ToDoList.Api.Validator
{
    public class ToDoListValidator : AbstractValidator<ToDoListDetailsQueryDto>
    {
        public ToDoListValidator()
        {
            RuleFor(m => m.Name).NotEmpty();
            RuleFor(m => m.Name).MinimumLength(2);
        }
    }
}
