﻿using GE.Business;
using GE.Business.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GE.ToDoList.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoListController : ControllerBase
    {
        readonly IToDoListRepository _toDoListRepository;
        private readonly string _commonErrorMessage = "Something went wrong ,please try after sometime";

        public ToDoListController(IToDoListRepository toDoListRepository)
        {
            _toDoListRepository = toDoListRepository;
        }

        /// <summary>
        /// Get All Todo List
        /// </summary>
        /// <returns></returns>
        // GET /api/ToDoList/GetAllToDoList
        [HttpGet("GetAllToDoList")]
        public async Task<ActionResult> Get()
        {
            try
            {
                var result = await _toDoListRepository.GetAllToDoList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(_commonErrorMessage + ex.Message);
            }
        }

        /// <summary>
        /// Get Todo List Details
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>

        [HttpGet("GetToDoListDetails")]
        public async Task<ActionResult> GetToDoListDetails(Guid itemId)
        {
            try
            {
                if (itemId == Guid.Empty)
                {
                    throw new Exception("Invalid Item Id");
                }
                var result = await _toDoListRepository.GetToDoListById(itemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(_commonErrorMessage + ex.Message);
            }
        }

        /// <summary>
        /// Add Todo List
        /// </summary>
        /// <param name="toDoListQueryDto"></param>
        /// <returns></returns>

        // POST /api/ToDoList/CreateToDoList
        [HttpPost("CreateToDoList")]
        public async Task<ActionResult> Post([FromBody] ToDoListDetailsQueryDto toDoListQueryDto)
        {
            try
            {
                var result = await _toDoListRepository.Add(toDoListQueryDto);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(_commonErrorMessage + ex.Message);
            }
        }

        /// <summary>
        /// Complete Todo List
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // PUT /api/ToDoList/CompleteToDoList/5
        [HttpPut("CompleteToDoList/{id}")]
        public async Task<ActionResult> Put(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    throw new Exception("Invalid Item Id");
                }
                var result = await _toDoListRepository.Update(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(_commonErrorMessage + ex.Message);
            }
        }
    }
}
