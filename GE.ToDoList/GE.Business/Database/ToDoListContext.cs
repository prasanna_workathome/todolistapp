﻿using GE.Business.Dtos;
using Microsoft.EntityFrameworkCore;

namespace GE.Business.Database
{
    public class ToDoListContext : DbContext
    {
        public DbSet<ToDoListItemBaseDto> ToDoListItemBaseDto { get; set; }
        public DbSet<ToDoListQueryDto> ToDoListQueryDto { get; set; }
        public DbSet<ToDoListDetailsQueryDto> ToDoListDetailsQueryDto { get; set; }
      
        public ToDoListContext(DbContextOptions<ToDoListContext> options) : base(options)
        {

        }
    }
}
