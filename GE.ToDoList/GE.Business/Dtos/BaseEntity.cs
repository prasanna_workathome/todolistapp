﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GE.Business.Dtos
{
    public class BaseEntity : IAuditFields
    {
        [Obsolete]
        [Key]
        public Guid Id { get; set; }
        [Obsolete]
        public string CreatedById { get; set; }
        [Obsolete]
        public string ModifiedById { get; set; }
        [Obsolete]
        public DateTime CreatedOn { get ; set; }
        [Obsolete]
        public DateTime ModifiedOn { get ; set ; }
    }
}
