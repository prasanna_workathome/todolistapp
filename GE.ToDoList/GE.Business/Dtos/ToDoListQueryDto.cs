﻿using System;
using Microsoft.EntityFrameworkCore;

namespace GE.Business.Dtos
{
    [Keyless]
    public class ToDoListQueryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
