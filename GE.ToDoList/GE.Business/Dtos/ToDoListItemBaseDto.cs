﻿namespace GE.Business.Dtos
{
    public class ToDoListItemBaseDto : BaseEntity
    {
        public string Name { get; set; }
        public bool IsCompleted { get; set; }
    }
}
