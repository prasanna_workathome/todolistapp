﻿using AutoMapper;

namespace GE.Business.Dtos
{
    internal static class CreateMap
    {
        public static void Map(IProfileExpression config)
        {
            config.CreateMap<ToDoListItemBaseDto, ToDoListQueryDto>()
                .ForSourceMember(x => x.IsCompleted, opt => opt.DoNotValidate())
                .PreserveReferences();
            config.CreateMap<ToDoListItemBaseDto, ToDoListDetailsQueryDto>()
                .PreserveReferences();
            config.CreateMap<ToDoListDetailsQueryDto, ToDoListItemBaseDto>()
                .PreserveReferences();
            config.CreateMap<ToDoListQueryDto, ToDoListItemBaseDto>()
                .ForMember(x => x.IsCompleted, opt => opt.Ignore())
                .ForMember(x => x.CreatedById, opt => opt.Ignore())
                .ForMember(x => x.ModifiedById, opt => opt.Ignore())
                .ForMember(x => x.ModifiedOn, opt => opt.Ignore())
                .ForMember(x => x.CreatedOn, opt => opt.Ignore())
                .PreserveReferences();
        }
    }
}
