﻿using System;

namespace GE.Business.Dtos
{
    /// <summary>
    /// Interface for entities that provide audit fields.
    /// </summary>
    public interface IAuditFields
    {
        string CreatedById { get; set; }
        DateTime CreatedOn { get; set; }
        string ModifiedById { get; set; }
        DateTime ModifiedOn { get; set; }
    }
}
