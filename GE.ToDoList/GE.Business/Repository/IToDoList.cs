﻿using GE.Business.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GE.Business.Repository
{
    public interface IToDoListRepository
    {
        Task<ToDoListDetailsQueryDto> Add(ToDoListDetailsQueryDto toDoListDetailsQueryDto);
        Task<ToDoListDetailsQueryDto> Update(Guid id);
        Task<List<ToDoListQueryDto>> GetAllToDoList();
        Task<ToDoListDetailsQueryDto> GetToDoListById(Guid id);
    }
}
