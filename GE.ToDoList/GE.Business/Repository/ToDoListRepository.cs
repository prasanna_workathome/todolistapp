﻿using AutoMapper;
using GE.Business.Database;
using GE.Business.Dtos;
using GE.Business.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GE.Business.Repository
{
    public class ToDoListRepository : IToDoListRepository
    {
        private readonly ToDoListContext _context;
        private readonly IMapper _mapper;
        private const string SystemAdmin = "System Admin";

        public ToDoListRepository(ToDoListContext context)
        {
            MappingManager.Initialize();
            _mapper = MappingManager.AutoMapper;
            _context = context;
        }

        /// <summary>
        /// Add TODO List
        /// </summary>
        /// <param name="toDoListDetailsQueryDto"></param>
        public async Task<ToDoListDetailsQueryDto> Add(ToDoListDetailsQueryDto toDoListDetailsQueryDto)
        {
            try
            {
                var toDoListBaseDto = new ToDoListItemBaseDto();
                _mapper.Map(toDoListDetailsQueryDto, toDoListBaseDto);

                toDoListBaseDto = InitializeAuditFields(toDoListBaseDto);
                await _context.ToDoListItemBaseDto.AddAsync(toDoListBaseDto);

                await _context.SaveChangesAsync();
                toDoListDetailsQueryDto.Id = toDoListBaseDto.Id;

                return toDoListDetailsQueryDto;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Update TODO List
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ToDoListDetailsQueryDto> Update(Guid id)
        {
            var toDoListDetailsQueryDto = new ToDoListDetailsQueryDto();
            var toDoListBaseDto = _context.ToDoListItemBaseDto.FirstOrDefault(x => x.Id == id);

            if (toDoListBaseDto != null)
            {
                toDoListBaseDto.IsCompleted = true;
                toDoListBaseDto.ModifiedOn = DateTime.Now;
                _context.ToDoListItemBaseDto.Update(toDoListBaseDto);
            }

            await _context.SaveChangesAsync();
            _mapper.Map(toDoListBaseDto, toDoListDetailsQueryDto);

            return toDoListDetailsQueryDto;
        }

        /// <summary>
        /// Get All TODO List
        /// </summary>
        /// <returns></returns>
        public async Task<List<ToDoListQueryDto>> GetAllToDoList()
        {
            try
            {
                var allToDoList = await _context.ToDoListItemBaseDto.ToListAsync();
                var response = _mapper.Map<List<ToDoListQueryDto>>(allToDoList);
                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Get ToDoList ById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ToDoListDetailsQueryDto> GetToDoListById(Guid id)
        {
            try
            {
                var allToDoList = await _context.ToDoListItemBaseDto.FirstOrDefaultAsync(x => x.Id == id);
                var toDoListQueryDetailsDto = new ToDoListDetailsQueryDto();
                var result = _mapper.Map(allToDoList, toDoListQueryDetailsDto);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #region PrivateMethod

        private static T InitializeAuditFields<T>(T entity) where T : BaseEntity
        {
            entity.Id = Guid.NewGuid();
            entity.CreatedById = SystemAdmin;
            entity.ModifiedById = SystemAdmin;
            entity.CreatedOn = DateTime.Now;
            entity.ModifiedOn = DateTime.Now;
            return entity;
        }

        #endregion
    }
}
