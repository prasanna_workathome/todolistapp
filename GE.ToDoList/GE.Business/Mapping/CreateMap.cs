﻿using AutoMapper;

namespace GE.Business.Mapping
{
    internal static class CreateMap
    {
        internal static void Map(IProfileExpression config)
        {
            Dtos.CreateMap.Map(config);
        }
    }
}
