﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GE.Business.CosmosDB
{
    public class Employee
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Address Address { get; set; }
    }
}
