﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;

namespace GE.Business.CosmosDB
{
    public interface ICosmosDbRepository
    {
        Task<Microsoft.Azure.Cosmos.Database> CreateDatabaseAsync();
        Task<Container> CreateContainerAsync();
        Task<Employee> AddItemsToContainerAsync(Employee emp);
        Task<List<Employee>> QueryItemsAsync(string sqlQueryText);
        Task CreateCosmosDb();
    }
}
