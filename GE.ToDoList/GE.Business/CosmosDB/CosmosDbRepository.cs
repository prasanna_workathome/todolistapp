﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;

namespace GE.Business.CosmosDB
{
    public class CosmosDbRepository : ICosmosDbRepository
    {
        private CosmosClient cosmosClient;
        private Microsoft.Azure.Cosmos.Database database;
        private static Container _container;
        private string databaseId = "PrasannaHomeDatabase";
        private string containerId = "PrasannaHomeContainer";
        private readonly IConfiguration _config;

        public CosmosDbRepository(IConfiguration config)
        {
            _config = config;
            var endpointUrl = _config.GetSection("EndpointUrl").Value;
            var primaryKey = _config.GetSection("PrimaryKey").Value;
            cosmosClient = new CosmosClient(endpointUrl, primaryKey);
        }
       
        public async Task<Microsoft.Azure.Cosmos.Database> CreateDatabaseAsync()
        {
            try
            {
                database = await cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);
                return database;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           
        }

        public async Task<Container> CreateContainerAsync()
        {
            try
            {
                _container = await database.CreateContainerIfNotExistsAsync(containerId, "/LastName");
                return _container;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
          
        }

        public async Task<Employee> AddItemsToContainerAsync(Employee emp)
        {
            try
            {
                 database = await cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);
                _container = await database.CreateContainerIfNotExistsAsync(containerId, "/LastName");
                var andersenFamilyResponse = await _container.CreateItemAsync(emp, new PartitionKey(emp.LastName));
                return andersenFamilyResponse;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<Employee>> QueryItemsAsync(string sqlQueryText)
        {
            database = await cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId);
            _container = await database.CreateContainerIfNotExistsAsync(containerId, "/LastName");
            List<Employee> employees = new List<Employee>();
            QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);
            FeedIterator<Employee> queryResultSetIterator = _container.GetItemQueryIterator<Employee>(queryDefinition);
            while (queryResultSetIterator.HasMoreResults)
            {
                FeedResponse<Employee> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                foreach (Employee emp in currentResultSet)
                {
                    employees.Add(emp);
                }
            }

            return employees;
        }

        public async Task CreateCosmosDb()
        {
            try
            {
               await CreateDatabaseAsync();
               await CreateContainerAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
