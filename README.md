# README #

This README would normally document whatever steps are necessary to get your application up and running.

### IDE AND Version ###

* IDE Details : VISUAL STUDIO 2019 , Version 16.8.3
* Version : .NET CORE VERSION : .NET 5.0
* OPEN API : SWAGGER Documentation

### How do I get set up? ###

* Code is available in public bitbucket repo : https://bitbucket.org/prasanna_hach/todolistapp/src/master/
* To get code in local using this git command : git clone https://prasanna_hach@bitbucket.org/prasanna_hach/todolistapp.git in command prompt
* Web Api will open with URL : https://localhost:44321/swagger/index.html , you can change this port number from lunch settings.json file inside properties folder
* Set `GE.ToDoList.Api` project as start up Project

### HOW TO TEST : ###
- STEP 1: FOLLOW SWAGGER UI TO TEST API OPENED IN BROWSER https://localhost:44321/swagger/index.html
- STEP 2 : FOLLOW CURL URL TO TEST API
### HTTPS URLS : ###
- CreateToDoList      : https://localhost:44327/swagger/index.html//api/ToDoList/CreateToDoList
- GetAllToDoList      : https://localhost:44327/swagger/index.html/api/ToDoList/GetAllToDoList
- GetToDoListDetails  : https://localhost:44327/swagger/index.html/api/ToDoList/GetToDoListDetails
- CompleteToDoList    : https://localhost:44327/swagger/index.html/api/ToDoList/CompleteToDoList/98b264cc-6ec9-4dbc-9511-e21d475701a1

### Data base configuration : ###
- This project used Entity framework core InMemory Database

### ARCHITECTURE & REQUEST FLOW: ###
*  There are three projects Business ,API and unit test 
*  Business has Databse ,Dtos ,Mapping and Repository Folders
*  Databse has DbContext file which is entity framework core 
*  Dtos folder has all required models and Domain Entity
*  Mapping folers has functionality to map business entity to Domain Entity and it used Auto mapper framework
*  Repository is abstraction layer and has all SCRUD operations required by application to InMemory Databse
*  API project has controller which has required end points for the application
*  API projects used .Net Core Fluent API for validation and validation class present inside validator folder
*  Unit test projects used NUnit Framework to test Business layer functionality

### How to run tests ###
 * Open Visual studio and from Test Menu on top click on Run all test

### Contribution guidelines ###

* NUnit Framework used to write unit test

### Who do I talk to? ###

* Public bitbucket repo : https://bitbucket.org/prasanna_hach/todolistapp/src/master/

### NOTES : ###
*  Marked few models property as Obsolete not to display in swagger API Documents